package OOP;

public class teacher extends person{

	public teacher(String Vname, String Vbirthday, double Vcmnd, double salary) {
		super(Vname, Vbirthday, Vcmnd);
		this.salary = salary;
	}

	public double salary;
	
	public void set_salary(double salary)
	{
		this.salary=salary;
	}
	
	public double get_salary()
	{
		return this.salary;
	}
	
	public void showInfo()
	{
		super.showInfo();
		System.out.println("Luong thang: " + this.salary);
		System.out.println("");
	}
}
