package OOP;

import java.util.Calendar;

public class person {
	public String name;
	public String birthday;
	public double cmnd;
	
	public person(String Vname,String Vbirthday,double Vcmnd)
	{
		this.name=Vname;
		this.birthday=Vbirthday;
		this.cmnd=Vcmnd;
	}
	
	public void set_name(String name)
	{
		this.name=name;
	}
	
	public String get_name()
	{
		return this.name;
	}
	
	public void set_cmnd(double cmnd)
	{
		this.cmnd=cmnd;
	}
	
	public double get_cmnd()
	{
		return this.cmnd;
	}
	
	public void set_birthday(String birthday)
	{
		this.birthday=birthday;
	}
	
	public String get_birthday()
	{
		return this.birthday;
	}
	
	public String get_day()
	{
		int date  = 0;
		int month = 0;
		int year  = 0;
		String day;
		Calendar now = Calendar.getInstance();
		date  = now.get(Calendar.DATE);
		month = now.get(Calendar.MONTH);
		year  = now.get(Calendar.YEAR);
		day = date + "/" + month + "/" + year;
		return day;
	}
	
	public void showInfo()
	{
		System.out.println("Ten: " + this.name);
		System.out.println("Ma: " + this.cmnd);
		System.out.println("Ngay them: " + get_day());
	}
}
