package OOP;

public class student extends person{
	public student(String Vname, String Vbirthday, double Vcmnd, double score) {
		super(Vname, Vbirthday, Vcmnd);
		this.score = score;
	}

	public double score;
	
	public void set_score(double score)
	{
		this.score=score;
	}
	
	public double get_score()
	{
		return this.score;
	}
	
	public void showInfo()
	{
		System.out.println("");
		super.showInfo();
		System.out.printf("Diem: " + this.score);
	}
}
